<?php

class Place
{
    protected $begin;
    protected $end;
    protected $name;
    protected $description;

    function __construct($begin, $end, $name, $description)
    {
        $this->begin = $begin;
        $this->end = $end;
        $this->name = $name;
        $this->description = $description;
    }
}

class Education extends Place
{
    public $city;

    function __construct($begin, $end, $name, $description, $city)
    {
        parent::__construct($begin, $end, $name, $description);
        $this->city = $city;
    }
}

class Work extends Place
{
    public $whoami;

    function __construct($begin, $end, $name, $description, $whoami)
    {
        parent::__construct($begin, $end, $name, $description);
        $this->whoami = $whoami;
    }
}

class Project extends Work
{
    public $url;

    function __construct($begin, $end, $name, $description, $url, $whoami = "")
    {
        parent::__construct($begin, $end, $name, $description, $whoami);
        $this->url = $url;
    }
}

class Candidate
{
    const ADVANTAGES = "преимущества";
    const DISADVANTAGES = "недостатки";

    const KNOW = "знаю";
    const WANTTOKNOW = "хотел бы выучить";

    protected $first_name;
    protected $last_name;
    protected $born_date;
    private $live;

    // education + works + projects
    protected $places = null;
    protected $characteristics = null;
    protected $knowledge = null;

    public $few_words_about = "";

    public $contacts = null;

    function __construct($first_name, $last_name, $born, $live = "")
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->born_date = $born;
        $this->live = $live;

        $this->places = new ArrayObject();
        $this->characteristics = [];
        $this->knowledge = [];
        $this->contacts = [];
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function addPlace(Place $value)
    {
        $this->places->append($value);
    }

    public function addCharacteristic($type, $value)
    {
        if (is_array($value)) {
            foreach ($value as $v) {
                $this->characteristics[$type][] = $v;
            }
            // array_combine($this->characteristics, $value);
        } else {
            $this->characteristics[$type][] = $value;
        }
    }

    public function addKnowledge($type, $value)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $this->knowledge[$type][$k] = $v;
            }
        } else {
            $this->knowledge[$type][] = $value;
        }
    }

    public function sayHello()
    {
        echo "Привет! Меня зовут {$this->first_name} {$this->last_name}." . PHP_EOL;
    }

}

$kirill = new Candidate(
    "Кирилл",
    "Марамыгин",
    new DateTime('1986-08-19', new DateTimeZone('Asia/Novosibirsk')),
    "Москва, Зеленоград"
);

$kirill->addPlace(
    new Education(
        "1993",
        "1999",
        "Гимназия №148 им Сервантеса",
        "углубленное изучение испанского языка",
        "Санкт-Петербург"
    )
);

$kirill->addPlace(
    new Education("1999", "2003", "Гимназия №1528", "экспериментальный физ-мат класс", "Москва, Зеленоград")
);

$kirill->addPlace(
    new Education("2003", "2004", "МГТУ им Баумана ФН-1", "перевелся в Москву после первого курса", "Калуга")
);

$kirill->addPlace(
    new Education(
        "2004",
        "2009",
        "МГТУ им Баумана ИУ7",
        "специальность \"Информатика и вычислительная техника\"",
        "Москва"
    )
);

$kirill->addCharacteristic(Candidate::ADVANTAGES, 'определенная аккуратность');
$kirill->addCharacteristic(
    Candidate::ADVANTAGES,
    [
        'довожу дело до конца',
        'неплохая обучаемость',
    ]
);
$kirill->addCharacteristic(
    Candidate::DISADVANTAGES,
    [
        'не очень усидчивый',
        'наверное склонен к прокрастинацие',
    ]
);

$kirill->addPlace(new Work("2007", "2007", "GoldenTelecom", "решал проблемы техподдержки", "специалист техподдержки"));

$kirill->addPlace(
    new Work(
        "2007",
        "2013",
        "SEO",
        "На последних курсах стал заниматься SEO, неплохо получалось (а тогда вообще все легко было) и деньги нормальные.",
        "специалист по поисковой оптимизации"
    )
);

$kirill->addPlace(
    new Work(
        "2010",
        "2014",
        "csltrade.ru",
        "В 2010 году начал помогать знакомым в их нелегком e-commerce бизнесе - csltrade.ru. Сначала по SEO, потом по всему, что приходило в голову :), например: SEO + интернет-маркетинг, проектирование сайтов, увеличение конверсий, настройка серверов. Занимался доработкой и переработкой их самописной CMS, писал внутренние скрипты, немного руководил людьми-дизайнерами/копирайтерами/менеджерами_по_контекстной_рекламе/программистами. Даже в 1С пришлось немного разобраться :)",
        "инженер/программист/системный администратор/технический директор/интернет-макетолог/руководитель отдела"
    )
);

$kirill->addPlace(
    new Work(
        "2014",
        "2014",
        "webasyst.ru",
        "Работал над парой приложений системы webasyst.",
        "программист"
    )
);

$kirill->addCharacteristic(
    Candidate::ADVANTAGES,
    [
        'много помню',
        'во многом разбираюсь',
        'открыт новым зананиям',
        'готов принимать решения',
        'внимателен к чужому мнению',
        'люблю автоматизировать процессы',
    ]
);

$kirill->addCharacteristic(
    Candidate::DISADVANTAGES,
    [
        'знания не глубокие',
        'долго выбираю оптимальный метод решения задачи',
    ]
);

$kirill->addPlace(
    new Project(
        "2013",
        "2013",
        "Скрипт оптового заказа",
        "Небольшой скрипт для загрузки номенклатуры из Excel и потом заказ через сайт (javascript, handlebarsjs, php)",
        "http://fpl.ru"
    )
);

$kirill->addPlace(
    new Project(
        "2012",
        "2012",
        "Простой сайт с небольшой админкой",
        "Обычный сайт с минимальной админкой. Решил попробовать микрофреймоврк FatFreeFramework. Сайт сайт не тот уже :(",
        "http://armamodul.ru"
    )
);

$kirill->addPlace(
    new Project(
        "2013",
        "2013",
        "Создание небольшого сайта-каталога",
        "Выбрал prestashop как исходный вариант. Натянул дизайн на шаблоны, подредактировал код.",
        "http://indigo-optic.ru"
    )
);

$kirill->addPlace(
    new Project(
        "2013",
        "2013",
        "Доработка интернет-магазина",
        "Нужно было доделать варианты доставка, оплаты, подключить эквайринг. Сайт сделан на joomla.",
        "http://wall-print.ru"
    )
);

$kirill->addPlace(
    new Project(
        "2013",
        "2014",
        "Интенет-магазин нового русского бренда мебели",
        "Курировал создание сайта для нового русского дизайнерского бренда мебели. Самописная CMS на Ruby on Rails. Плотно работал с дизайнером, верстальщиком, еще одним программистом.",
        "http://lllooch.ru",
        "руководитель проекта"
    )
);

$kirill->addPlace(
    new Project(
        "2013",
        "2014",
        "CALLIE Roaming",
        "Создание проекта туристистических SIM карт. Создание сайта с использованием фреймворка Yii. Верстка на bootstrap. Проект загнулся.",
        "http://callie.haliavka.com/",
        "технический директор"
    )
);

$kirill->addPlace(
    new Project(
        "2014",
        "2014",
        "Towers of Hanoi",
        "Тестовое задание на вакансию веб-разработчика в webasyst.",
        "http://hanoi.maramygin.ru"
    )
);
$kirill->addPlace(
    new Project(
        "2014",
        "2014",
        "WebAsyst Mailer",
        "Доделовал и модернизировал приложение «Рассылки» для email маркетинга.",
        "http://www.webasyst.ru/store/app/mailer/"
    )
);
$kirill->addPlace(
    new Project(
        "2014",
        "2014",
        "WebAsyst Контакты PRO",
        "Плагин для создания CRM. Помогал в разработке плагина для расширенной работы с контактами.",
        "http://www.webasyst.ru/store/plugin/contacts/pro/"
    )
);
$kirill->addPlace(
    new Project(
        "2014",
        "2014",
        "waSlideMenu",
        "jQuery-плагин, который превращает древовидное (иерархическое) структуру в систему прокручивающихся меню.",
        "https://github.com/webasyst/waslidemenu"
    )
);

$kirill->addCharacteristic(
    Candidate::ADVANTAGES,
    [
        'люблю читать про новые технологии',
        'много читаю статей',
        'люблю применять что-то новое',
        'могу разобраться в чужом коде',
        'люблю писать красиво',
        'люблю комментарии',
    ]
);

$kirill->addCharacteristic(
    Candidate::DISADVANTAGES,
    [
        'бывает туплю',
        'бывает задаю очень глупые вопросы',
        'не умею правильно тестировать',
        'собственное мнение',
        'мало оптыта работы в команде',
        'не умею применять паттерны',
        'самокритичен',
    ]
);

$kirill->addKnowledge(
    Candidate::KNOW,
    [
        'PHP' => 3.5,
        'JavaScript' => 3.5,
        'MySQL' => 3.0,
        'jQuery' => 3.8,
        'Bootstrap' => 3.1,
        'Git' => 2.0,
        'Mercurial' => 2.1,
        'Yii' => 2.5,
        'Unix' => 3.5,
        'Nginx' => 3.0,
        'handlebars.js' => 3.0,
    ]
);

$kirill->addKnowledge(
    Candidate::WANTTOKNOW,
    [
        'TDD' => 0.5,
        'работа в команде' => 1.0,
        'применение паттернов' => 0,
        'highload' => 0,
        'Django' => 0.5,
        'RoR' => 1,
        'Objective C' => 0.1,
    ]
);

$kirill->few_words_about = "Я долго работал сам с собой (ну или со знакомыми). Сейчас мне хочется поработать в команде, поучиться у других, понят свои слабые стороны, поучаствовать в действительно большом проекте.)";

$kirill->contacts = [
    'email' => 'ineedjob@maramygin.ru',
    'phone' => 'just mail me',
    'repo' => ['https://bitbucket.org/kerstvo', 'https://github.com/kerstvo'],
    'urls' => ''
];

$kirill->sayHello();

var_dump($kirill);
